# aws_iot_certificate cert
resource "aws_iot_certificate" "cert" {
  active = true
  # Spécifie ici les autres attributs requis pour le certificat
}

# aws_iot_policy pub-sub
resource "aws_iot_policy" "pub_sub_policy" {
  name = "pub-sub-policy"

  # Utilise le module file pour charger le contenu du fichier iot_policy.json
  policy = file("${path.module}/files/iot_policy.json")
}

# aws_iot_policy_attachment attachment
resource "aws_iot_policy_attachment" "attachment" {
  policy = aws_iot_policy.pub_sub_policy.name
  target = aws_iot_certificate.cert.arn
}

# aws_iot_thing temp_sensor
resource "aws_iot_thing" "temp_sensor" {
  name = "temp-sensor"
  # Spécifie ici d'autres attributs requis pour le thing (objet connecté)
}

# aws_iot_thing_principal_attachment thing_attachment
resource "aws_iot_thing_principal_attachment" "thing_attachment" {
  thing = aws_iot_thing.temp_sensor.name
  principal = aws_iot_certificate.cert.arn
}

# data aws_iot_endpoint pour récupérer l'endpoint à appeler dans simulation.py
data "aws_iot_endpoint" "iot_endpoint" {
  endpoint_type = "iot:Data-ATS"
}

# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "temperature_to_dynamodb" {
  name        = "TemperatureToDynamoDB"
  sql         = "SELECT * FROM 'sensor/temperature/+'"
  description = "Rule to send temperature data to DynamoDB"
  enabled     = true
  sql_version = "2016-03-23"

  # Configuration de l'action DynamoDBv2
  dynamodbv2 {
    role_arn = aws_iam_role.iot_role.arn  # Remplacez par le rôle "iot_role"
    put_item {
      table_name = aws_dynamodb_table.Temperature.name  # Remplacez par le nom de la table "Temperature"
      
    }
  }
}

# ... (autres ressources AWS IoT Core existantes restent inchangées)




# aws_iot_topic_rule rule for sending valid data to Timestream


###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
